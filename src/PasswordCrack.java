import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.util.Scanner;



public class PasswordCrack {
	public static void main (String[] args) throws IOException{
		
		String dictionary = args[0];
		String passwd = args[1];
		File pw = new File(passwd);
		Scanner sc = new Scanner(pw);
		//int k = Integer.parseInt(args[2]);
				
				
		InputStream in = new FileInputStream(pw);
		@SuppressWarnings("resource")
		Reader reader = new InputStreamReader(in);
		        
		String arr[] = new String [20];
		int count = 0;
		int a;
		char b = 0;
		char c = 0;
		char d = 0;
		char e = 0;
		char f = 0;
		int semi = 6;
		String password = null;
	
		@SuppressWarnings("resource")
		RandomAccessFile file = new RandomAccessFile((pw), "r");
		long index, length;
		length = file.length() - 1; 
		
		long startTime = System.currentTimeMillis();
		System.out.println("Cracked Passwords: ");
		for (index = 0; index < length; index++) 
		{
			a = reader.read();
			if(a == 58)
			{
				if(semi == 6)
				{
					file.seek(index+1);
					b = (char) file.read();
					file.seek(index+2);
					c = (char) file.read();
					semi = 0;
					file.seek(index + 3);
					d = (char) file.read();
					password = Character.toString(d);
					String salt = Character.toString(b) + Character.toString(c);
					String firstName = null;
					String lastName = null;
					
					for(int i = 0; i < 10; i++)
					{
						file.seek(index + 4 + i);
						d = (char) file.read();
						password = password + Character.toString(d);
					}
					int j = 0;
					file.seek(index + 23);
					f = (char)file.read();
					firstName = Character.toString(f);
					
					while(true){
						file.seek(index + 24 + j);
						f = (char)file.read();
						j++;
						if(f == ' ')
							break;
						firstName = firstName +Character.toString(f);
					}
					
					file.seek(index + 24 + j);
					e = (char)file.read();
					lastName = Character.toString(e);
					int k = j;
					while(true){
						file.seek(index + 25 + k);
						e = (char)file.read();
						k++;
						if(e == ':')
							break;
						lastName = lastName +Character.toString(e);
					}
					
					//System.out.println(firstName + " " + lastName);
					
					String encrypted = salt + password;
					//System.out.println(salt + " " + password + " " + encrypted + " ");
					
					FileInputStream fis = new FileInputStream(dictionary);
					//Construct BufferedReader from InputStreamReader
					BufferedReader br = new BufferedReader(new InputStreamReader(fis));
				 
					int name = 0;
					String compare = null;
					String word = null;
					char firstLetter;
					A: while (true) 
					{
						word = br.readLine();
						if(word == null)
						{
							if(name == 2)
								break;
							if(name == 1)
							{
								word = lastName;
								name++;
								//System.out.println(lastName + " " + name);
							}
							if(name == 0)
							{
								word = firstName;
								name++;
								//System.out.println(firstName + " " + name);
							}
						}
						
						
						String words = word;
						StringBuilder sb = new StringBuilder(word);
						
							//take as is
							compare = jcrypt.crypt(salt, sb.toString());
							//System.out.println("Compare: " + compare);
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
						
							//uppercase the string, e.g., STRING;
							word = words;
							word = word.toUpperCase();
							compare = jcrypt.crypt(salt, word);
							if (compare.equals(encrypted)){
								arr[count] = word;
								count++;
								System.out.println(word + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
						
							//lowercase the string, e.g., string;
							word = words;
							word = word.toLowerCase();
							compare = jcrypt.crypt(salt, word);
							if (compare.equals(encrypted)){
								arr[count] = word;
								count++;
								System.out.println(word + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
						
							//delete the first character from the string, e.g., tring;
							word = words;
							sb = new StringBuilder(word);
							sb.deleteCharAt(0);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
						
							//delete the last character from the string, e.g., strin;
							word = words;
							sb = new StringBuilder(word);
							sb.deleteCharAt(sb.length() - 1);
							//System.out.println(sb);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							
							//capitalize the string, e.g., String;
							word = words;
							sb = new StringBuilder(word);
							firstLetter = sb.charAt(0);
							firstLetter = (char) (firstLetter - 32);
							sb.deleteCharAt(0);
							sb.insert(0, firstLetter);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							
							//ncapitalize the string, e.g., sTRING;
							word = words;
							sb = new StringBuilder(word);
							firstLetter = sb.charAt(0);
							if (firstLetter >= 65 && firstLetter <= 90){
								firstLetter += 32;
							}
							sb.deleteCharAt(0);
							for (int i = 0; i < sb.length(); i++) {
							    char ch = sb.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb.insert(0, firstLetter);
							//System.out.println(sb);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}

							

							//toggle case of the string, e.g., StRiNg or sTrInG;
							word = words;
							sb = new StringBuilder(word);
							
							for (int i = 0; i < sb.length(); i++) {
							    char ch = sb.charAt(i);
							    if (i % 2 != 0) {
							        sb.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							word = words;
							sb = new StringBuilder(word);
							
							for (int i = 0; i < sb.length(); i++) {
							    char ch = sb.charAt(i);
							    if (i % 2 == 0) {
							        sb.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							//reverse the string, e.g., gnirts;
							word = words;
							sb = new StringBuilder(word);
							sb.reverse();
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							//duplicate the string, e.g., stringstring;
							word = words;
							sb = new StringBuilder(word);
							sb.append(words);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word = words;
							sb = new StringBuilder(word);
							StringBuilder rv = new StringBuilder(word);
							rv.reverse();
							sb.append(rv);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							word = words;
							sb = new StringBuilder(word);
							StringBuilder rv2 = new StringBuilder(word);
							sb.reverse();
							sb.append(rv2);
							compare = jcrypt.crypt(salt, sb.toString());
							if (compare.equals(encrypted)){
								arr[count] = sb.toString();
								count++;
								System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break A;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word = words;
								sb = new StringBuilder(word);
								firstLetter = (char) (i);
								sb.insert(0, firstLetter);
								compare = jcrypt.crypt(salt, sb.toString());
								if (compare.equals(encrypted)){
									arr[count] = sb.toString();
									count++;
									System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break A;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word = words;
								sb = new StringBuilder(word);
								firstLetter = (char) (i);
								sb.append(firstLetter);
								compare = jcrypt.crypt(salt, sb.toString());
								if (compare.equals(encrypted)){
									arr[count] = sb.toString();
									count++;
									System.out.println(sb + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break A;
								}
							}
					}
				}
				semi++;
			}
		}
		
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~SECOND ROUND~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
		//System.out.println("~~~~~~~~~~~~~2~~~~~~~~~~~~~~~");
		File pw12 = new File(passwd);
		Scanner sc2 = new Scanner(pw12);
		
		InputStream in2 = new FileInputStream(pw12);
		@SuppressWarnings("resource")
		Reader reader2 = new InputStreamReader(in2);
		
		int a2;
		char b2 = 0;
		char c2 = 0;
		char d2 = 0;
		char e2 = 0;
		char f2 = 0;
		int semi2 = 6;
		String password2 = null;
		@SuppressWarnings("resource")
		RandomAccessFile file2 = new RandomAccessFile((pw12), "r");
		long index2, length2;
		length2 = file2.length() - 1; 
		for (index2 = 0; index2 < length2; index2++) 
		{
			a2 = reader2.read();
			if(a2 == 58)
			{
				if(semi2 == 6)
				{
					file2.seek(index2+1);
					b2 = (char) file2.read();
					file2.seek(index2+2);
					c2 = (char) file2.read();
					semi2 = 0;
					file2.seek(index2 + 3);
					d2 = (char) file2.read();
					password2 = Character.toString(d2);
					String salt2 = Character.toString(b2) + Character.toString(c2);
					String firstName2 = null;
					String lastName2 = null;
					
					for(int i = 0; i < 10; i++)
					{
						file2.seek(index2 + 4 + i);
						d2 = (char) file2.read();
						password2 = password2 + Character.toString(d2);
					}
					int j2 = 0;
					file2.seek(index2 + 23);
					f2 = (char)file2.read();
					firstName2 = Character.toString(f2);
					
					while(true){
						file2.seek(index2 + 24 + j2);
						f2 = (char)file2.read();
						j2++;
						if(f2 == ' ')
							break;
						firstName2 = firstName2 +Character.toString(f2);
					}
					
					file2.seek(index2 + 24 + j2);
					e2 = (char)file2.read();
					lastName2 = Character.toString(e2);
					int k2 = j2;
					while(true){
						file2.seek(index2 + 25 + k2);
						e2 = (char)file2.read();
						k2++;
						if(e2 == ':')
							break;
						lastName2 = lastName2 +Character.toString(e2);
					}
					
					String encrypted2 = salt2 + password2;
					//System.out.println(salt2 + " 2 " + password2 + " 2 " + encrypted2 + " 2 ");
					
					
					FileInputStream fis2 = new FileInputStream(dictionary);
					//Construct BufferedReader from InputStreamReader
					BufferedReader br2 = new BufferedReader(new InputStreamReader(fis2));
					
					int name2 = 0;
					String compare2 = null;
					String word2 = null;
					char firstLetter2;
					String temp = null;
					
					B: while (true) 
					{
						word2 = br2.readLine();
						if(word2 == null)
						{
							if(name2 == 2){
								//System.out.println(name2);
								break;
							}
							if(name2 == 1)
							{
								word2 = lastName2;
								name2++;
								//System.out.println(lastName2 + " " + name2);
							}
							if(name2 == 0)
							{
								word2 = firstName2;
								name2++;
								//System.out.println(firstName2 + " " + name2);
							}
						}
						String words2 = word2;
						StringBuilder sb2 = new StringBuilder(word2);
						StringBuilder rv = new StringBuilder(word2);
						StringBuilder rv2 = new StringBuilder(word2);
						
							//take as is
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
						
//uppercase the string, e.g., STRING;
							word2 = words2;
							word2 = word2.toUpperCase();
							temp = word2;
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							
//lowercase the string, e.g., string;
							word2 = words2;
							word2 = word2.toLowerCase();
							temp = word2;
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
						
//delete the first character from the string, e.g., tring;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							sb2.deleteCharAt(0);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
						
//delete the last character from the string, e.g., strin;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							sb2.deleteCharAt(sb2.length() - 1);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//capitalize the string, e.g., String;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							
//ncapitalize the string, e.g., sTRING;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}

							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}

//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							
							
//toggle case of the string, e.g., StRiNg or sTrInG;	
							word2 = words2;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//reverse the string, e.g., gnirts;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//duplicate the string, e.g., stringstring;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = words2;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							temp = sb2.toString();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								break B;
							}
							
							//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = temp;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									for(int j = 0; j < 20; j++){
										if((sb2.toString()).equals(arr[j])){
											break B;
										}
									}
									System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
									break B;
								}
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//prepend a character to the string, e.g., @string;
							for(int i = 32; i < 126; i++){
								word2 = words2;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.insert(0, firstLetter2);
								temp = sb2.toString();
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									break B;
								}
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
//append a character to the string, e.g., string9;
							for(int i = 32; i < 126; i++){
								word2 = words2;
								sb2 = new StringBuilder(word2);
								firstLetter2 = (char) (i);
								sb2.append(firstLetter2);
								temp = sb2.toString();
								compare2 = jcrypt.crypt(salt2, sb2.toString());
								if (compare2.equals(encrypted2)){
									break B;
								}
							}
							
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv = new StringBuilder(word2);
							rv.reverse();
							sb2.append(rv);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reflect the string, e.g., stringgnirts or gnirtsstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							rv2 = new StringBuilder(word2);
							sb2.reverse();
							sb2.append(rv2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//duplicate the string, e.g., stringstring;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.append(words2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//reverse the string, e.g., gnirts;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							sb2.reverse();
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 == 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//toggle case of the string, e.g., StRiNg or sTrInG;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (i % 2 != 0) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } else {
							        sb2.setCharAt(i, Character.toLowerCase(ch));
							    }
							}
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//ncapitalize the string, e.g., sTRING;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							if (firstLetter2 >= 65 && firstLetter2 <= 90){
								firstLetter2 += 32;
							}
							sb2.deleteCharAt(0);
							for (int i = 0; i < sb2.length(); i++) {
							    char ch = sb2.charAt(i);
							    if (Character.isLowerCase(ch)) {
							        sb2.setCharAt(i, Character.toUpperCase(ch));
							    } 
							}
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//capitalize the string, e.g., String;
							word2 = temp;
							sb2 = new StringBuilder(word2);
							firstLetter2 = sb2.charAt(0);
							firstLetter2 = (char) (firstLetter2 - 32);
							sb2.deleteCharAt(0);
							sb2.insert(0, firstLetter2);
							compare2 = jcrypt.crypt(salt2, sb2.toString());
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//lowercase the string, e.g., string;
							word2 = temp;
							word2 = word2.toLowerCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
							
							//uppercase the string, e.g., STRING;
							word2 = temp;
							word2 = word2.toUpperCase();
							compare2 = jcrypt.crypt(salt2, word2);
							if (compare2.equals(encrypted2)){
								for(int j = 0; j < 20; j++){
									if((sb2.toString()).equals(arr[j])){
										break B;
									}
								}
								System.out.println(sb2 + " - Time (ms): " + (System.currentTimeMillis() - startTime));
								break B;
							}
					}
				}
				semi2++;
			}
		}
	}
}
