UTEID: eab2982; dmd2479; dic223;
FIRSTNAME: Eddie; Daniel; Dylan;
LASTNAME: Babbe; Duncan; Inglis;
CSACCOUNT: dmduncan; babbe99; cinglis;
EMAIL: daniel.duncan@utexas.edu; babbe2012@utexas.edu; dylanmcquade@yahoo.com;

Note: Professor Young has given us permission to work as a team of 3. You can confirm with him if needed.

[Program 6] [Description]
There was really only a need to make 2 files for this assignment, which was PasswordCrack.java and jcrypt.java. PasswordCrack would take 2 arguments on the command line, the dictionary file (which is 3KB) and the file associated with the list of hashed passwords it needs to crack. It takes the password file, find the salt and the encrypted password associated with the specific user listed. It then takes the salt and goes through the list of words provided in the file called words. It takes these words as is as well as mangles them in algorithmic ways and encrypts each variation with the salt using an encryption function in jcrypt.java. It then compares each encrypted value with the encrypted password of the user and if it finds a match than it knows the password of the user. After it goes through each word it then tries the users first and last name with the salt to see if those are potential passwords as well. It will get the first round of mangled passwords within around 15 seconds (which is around 14 of the 20 passwords of the first test), then it will mangle the mangled passwords again to find more complex passwords and it finds some more (18 out of 20 passwords of the first test) within around a minute. 

[Finish] We have completely finished the assignment.

[Test Cases]

[Input of test 1]
java PasswordCrack words passwd1

[Output of test 1]
Cracked Passwords: 
michael - Time (ms): 520
amazing - Time (ms): 1010
eeffoc - Time (ms): 1087
squadro - Time (ms): 1426
icious - Time (ms): 1820
abort6 - Time (ms): 1822
rdoctor - Time (ms): 1951
doorrood - Time (ms): 2063
keyskeys - Time (ms): 2253
Impact - Time (ms): 2439
sATCHEL - Time (ms): 2770
THIRTY - Time (ms): 3156
enoggone - Time (ms): 4154
Salizar - Time (ms): 5391
liagiba - Time (ms): 16100
teserP - Time (ms): 46137
sHREWDq - Time (ms): 50300
obliqu3 - Time (ms): 55563

[Input of test 2]
java PasswordCrack words passwd2

[Output of test 2]
Cracked Passwords: 
tremors - Time (ms): 472
Saxon - Time (ms): 840
cOnNeLlY - Time (ms): 1244
eltneg - Time (ms): 1399
enchant$ - Time (ms): 1949
soozzoos - Time (ms): 2347
dIAMETER - Time (ms): 2453
ElmerJ - Time (ms): 2839
INDIGNITY - Time (ms): 3026
^bribed - Time (ms): 3082
ellows - Time (ms): 3513
zoossooz - Time (ms): 4365
Lacque - Time (ms): 25977
uPLIFTr - Time (ms): 60971
nosral - Time (ms): 65860
Swine3 - Time (ms): 75772
